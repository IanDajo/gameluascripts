-- Game Start Countdown v0.1 By =GG=DLBon

-- How many seconds does the countdown count backwards?
secondstocount = 15

-- Where should the messages be displayed? |c is the center |l is on the left |r is on the right.
displaywhere = "|r"

-- How many players need to be present for the timer to count to start the game? If not enough players are present no one will be able to move.
players = 1

-- Messages
waitmsg = "Please wait for more players."
timermsg = "Game starting in"
donetimermsg = "The game has started!"

-- Do not change anything below

api_version = "1.9.0.0"

hasstarted = {}
starttimer = {}
counter = {}
startpermi = {}
allowwait = {}
broadcasted = {}
divcount = {}

function OnScriptLoad()
	register_callback(cb["EVENT_TICK"],"Tick")
	register_callback(cb["EVENT_GAME_START"],"GameStart")
	timer(0100, "DisplayWait")
end

function GameStart()
	hasstarted = 0
	starttimer = 1
	counter = secondstocount * 40
	startpermi = 0
	allowwait = 0
	broadcasted = 0
end

function Tick()
	
	for i=1,16 do
		if(player_alive(i)) then
			local player = get_dynamic_player(i)

			if(i >= players and hasstarted == 0) then
				startpermi = 1
			end
			
			if(i <= players and hasstarted == 0) then
				allowwait = 1
			else
				allowwait = 0
			end
		end
	end
			
			if(startpermi == 1 and starttimer == 1 and counter ~= -1) then
				hasstarted = 1
				counter = counter - 1	
			end
			
			if(counter == 40 or counter == 80 or counter == 120 or counter == 160 or counter == 200 or counter == 240 or counter == 280 or counter == 320 or counter == 360) then
				divcount = counter / 40
				for i=1,16 do
					if(player_alive(i)) then
						local player = get_dynamic_player(i)
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "" .. timermsg .. " " .. divcount)
					end
				end
			end
			
			if(broadcasted == 0 and counter == 0) then
				for i=1,16 do
					if(player_alive(i)) then
						local player = get_dynamic_player(i)
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "")
						rprint(i, displaywhere .. "" .. donetimermsg)
					end
				end
				broadcasted = 1
				execute_command("sv_map_reset")
			end
			
			if(broadcasted == 0) then
				execute_command("lag *")
			end
			if(broadcasted == 1) then
				execute_command("unlag *")
			end
			
			

			
			
end

function DisplayWait()
	if(allowwait == 1) then
		for i=1,16 do
			if(player_alive(i)) then
				local player = get_dynamic_player(i)
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "")
				rprint(i, displaywhere .. "" .. waitmsg)
			end
		end
	end
	timer(0100, "DisplayWait")
end

function OnScriptUnload() end