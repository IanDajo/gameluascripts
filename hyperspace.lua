-- Player Hyper Space v0.1 By =GG=DLBon

-- How many hyper-space jumps does a player get? Default = 5
hyperspacejumps = 5

-- Messages:
join_msg = "Zoom in with a weapon to perform a Hyper-Space Jump!"
hyperspace_msg = "You have teleported to where you where looking! Hyper-Space Jumps left:"
hyperspaceusedup_msg = "You have no Hyper-Space Jumps left!"

-- Do not change anything below

api_version = "1.9.0.0"

ELSEACTION = {
	["else"] = 0,
}

zoom = {}
nowzoom = {}
zpress = {}
press = {}
space = {}
left = {}

function OnScriptLoad()
	register_callback(cb["EVENT_TICK"],"OnTick")
	register_callback(cb['EVENT_SPAWN'],"OnPlayerSpawn")
	register_callback(cb['EVENT_JOIN'],"JoinPlayer")
end

function OnPlayerSpawn(PlayerIndex)
	zoom[PlayerIndex] = 0
	nowzoom[PlayerIndex] = 0
	zpress[PlayerIndex] = 0
	press[PlayerIndex] = 1
	space[PlayerIndex] = hyperspacejumps
	left[PlayerIndex] = 0
end

function JoinPlayer(PlayerIndex)
	rprint(PlayerIndex,"|c" .. join_msg)
end

function OnTick()
	for i=1,16 do
		if(player_alive(i)) then
			local player = get_dynamic_player(i)
			
			zoom[i] = read_word(player + 0x480)
			
			if(zpress[i] == 0) then
			press[i] = 1
			else
			press[i] = 0
			end
			
			if(zoom[i] ~= nowzoom[i] and zpress[i] == 1 and space[i] > 0) then
				execute_command("boost " .. i)
				space[i] = space[i] - 1
				rprint(i,"|c" .. hyperspace_msg .. " " .. space[i])
				zpress[i] = 0
			else
				ELSEACTION["else"] = 0
			end
			if(zoom[i] ~= nowzoom[i] and zpress[i] == 0 and press[i] == 1 and space[i] > 0) then
				zpress[i] = 1
			else
				ELSEACTION["else"] = 0
			end
			if(space[i] == 0 and left[i] == 0) then
				rprint(i,"|c" .. hyperspaceusedup_msg)
				left[i] = 1
			end
			nowzoom[i] = zoom[i]

			
			
		end
	end
end

function OnScriptUnload() end