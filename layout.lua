-- Weapon layout v0.1 By =GG=DLBon

-- Should a player be allowed to choose a layout on repsawn again, or just when a new game starts or that player rejoins? Default = true
respawnchange = true

-- What weapons does each layout get?
-- layout 1:
layout1weap1 = "weapons\\assault rifle\\assault rifle"
layout1weap2 = "weapons\\pistol\\pistol"

-- layout 2:
layout2weap1 = "weapons\\sniper rifle\\sniper rifle"
layout2weap2 = "weapons\\rocket launcher\\rocket launcher"

-- What layout does a player spawn with by default? 1 = layout 1, 2 = layout 2. Default = 1
layouttospawnwith = 1

-- Messages:
nolayout_msg = "You can swap weapon layouts by pressing Q"
layoutchosen1_msg = "You can choose another layout after respawning."
layoutchosen2_msg = "You can choose another layout after rejoining."
layout1_msg = "You have chosen layout one!"
layout2_msg = "You have chosen layout two!"

-- Do not change anything below

api_version = "1.9.0.0"

ELSEACTION = {
	["else"] = 0,
}

nowshootingstate = {}
lastshootingstate = {}
flashlight = {}
nowflastlight = {}
hasshooten = {}
res = {}
qpress = {}
press = {}
layouttospawnwithswitch = {}
spawnswitch = {}
spawnweapon = {}
moreswitch = {}

function OnScriptLoad()
	register_callback(cb["EVENT_TICK"],"OnTick")
	register_callback(cb['EVENT_SPAWN'],"OnPlayerSpawn")
	register_callback(cb['EVENT_JOIN'],"EnableLoadoutonnewgame")
end

function OnPlayerSpawn(PlayerIndex)
	spawnweapon[PlayerIndex] = 0
	nowshootingstate[PlayerIndex] = 0
	lastshootingstate[PlayerIndex] = 0
	flashlight[PlayerIndex] = 0
	nowflastlight[PlayerIndex] = 0
	qpress[PlayerIndex] = 0
	press[PlayerIndex] = 1
	moreswitch[PlayerIndex] = 1
	if(respawnchange == true) then
		moreswitch[PlayerIndex] = 0
	end
	if(respawnchange ~= true and res[PlayerIndex] ~= 1) then
		hasshooten[PlayerIndex] = 0
	end
	if(hasshooten[PlayerIndex] == 0) then
		rprint(PlayerIndex,"|c" .. nolayout_msg)
	end
end

function EnableLoadoutonnewgame(PlayerIndex)
	hasshooten[PlayerIndex] = 0
	res[PlayerIndex] = 0
	spawnswitch[PlayerIndex] = layouttospawnwith
end

function OnTick()
	for i=1,16 do
		if(player_alive(i)) then
			local player = get_dynamic_player(i)
			
			flashlight[i] = read_bit(player + 0x208,4)
			
			if(layouttospawnwith == 1 and hasshooten[i] == 0 and spawnweapon[i] == 0) then
				execute_command("wdel " .. i)
				local x,y,z = read_vector3d(player + 0x5C)
				assign_weapon(spawn_object("weap",layout1weap1,x,y,z),i)
				assign_weapon(spawn_object("weap",layout1weap2,x,y,z),i)
				spawnweapon[i] = 1
			end
			if(layouttospawnwith == 2 and hasshooten[i] == 0 and spawnweapon[i] == 0) then
				execute_command("wdel " .. i)
				local x,y,z = read_vector3d(player + 0x5C)
				assign_weapon(spawn_object("weap",layout2weap1,x,y,z),i)
				assign_weapon(spawn_object("weap",layout2weap2,x,y,z),i)
				spawnweapon[i] = 1
			end
		
			if(spawnswitch[i] == 1 and hasshooten[i] == 1 and spawnweapon[i] == 0) then
				execute_command("wdel " .. i)
				local x,y,z = read_vector3d(player + 0x5C)
				assign_weapon(spawn_object("weap",layout1weap1,x,y,z),i)
				assign_weapon(spawn_object("weap",layout1weap2,x,y,z),i)
				spawnweapon[i] = 1
			end
			if(spawnswitch[i] == 2 and hasshooten[i] == 1 and spawnweapon[i] == 0) then
				execute_command("wdel " .. i)
				local x,y,z = read_vector3d(player + 0x5C)
				assign_weapon(spawn_object("weap",layout2weap1,x,y,z),i)
				assign_weapon(spawn_object("weap",layout2weap2,x,y,z),i)
				spawnweapon[i] = 1
			end
			
			nowshootingstate[i] = read_float(player + 0x490)
			if(nowshootingstate[i] ~= lastshootingstate[i] and nowshootingstate[i] == 1 and respawnchange == true and hasshooten[i] == 0) then
				hasshooten[i] = 1
				res[i] = 1
				rprint(i,"|c" .. layoutchosen1_msg)
			else
				ELSEACTION["else"] = 0
			end
			if(nowshootingstate[i] ~= lastshootingstate[i] and nowshootingstate[i] == 1 and respawnchange ~= true and hasshooten[i] == 0) then
				hasshooten[i] = 1
				res[i] = 1
				rprint(i,"|c" .. layoutchosen2_msg)
			else
				ELSEACTION["else"] = 0
			end
			lastshootingstate[i] = nowshootingstate[i]
			
			if(qpress[i] == 0) then
			press[i] = 1
			else
			press[i] = 0
			end
			
			if(flashlight[i] ~= nowflastlight[i] and flashlight[i] ~= 0 and hasshooten[i] == 0 and qpress[i] == 1) then
				rprint(i,"|c" .. layout1_msg)
				execute_command("wdel " .. i)
				local x,y,z = read_vector3d(player + 0x5C)
				assign_weapon(spawn_object("weap",layout1weap1,x,y,z),i)
				assign_weapon(spawn_object("weap",layout1weap2,x,y,z),i)
				qpress[i] = 0
				spawnswitch[i] = 1
			else
				ELSEACTION["else"] = 0
			end
			if(flashlight[i] ~= 0 and hasshooten[i] == 0 and qpress[i] == 0 and press[i] == 1) then
				rprint(i,"|c" .. layout2_msg)
				execute_command("wdel " .. i)
				local x,y,z = read_vector3d(player + 0x5C)
				assign_weapon(spawn_object("weap",layout2weap1,x,y,z),i)
				assign_weapon(spawn_object("weap",layout2weap2,x,y,z),i)
				qpress[i] = 1
				spawnswitch[i] = 2
			else
				ELSEACTION["else"] = 0
			end
			nowflastlight[i] = flashlight[i]
			
			if(moreswitch[i] == 0) then
				hasshooten[i] = 0
				moreswitch[i] = 1
			end

			
			
		end
	end
end

function OnScriptUnload() end