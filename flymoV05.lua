-- Vehicle Flymo v0.52 By =GG=DLBon

-- How fast should a player in a vehicle move up? Default = 0.07
movespeed = 0.07

-- Do not change anything below

api_version = "1.9.0.0"

ELSEACTION = {
	["else"] = 0,
}

currentjump = {}
previousjump = {}
vid = {}
permi = {}
wantedpos = {}
nowpos = {}
tpam = {}
timy = {}

function OnScriptLoad()
	register_callback(cb["EVENT_TICK"],"OnTick")
	register_callback(cb['EVENT_SPAWN'],"OnPlayerSpawn")
	register_callback(cb['EVENT_DAMAGE_APPLICATION'],"OnDamageApplication")
	register_callback(cb['EVENT_GAME_START'],"FallID")
end

function OnPlayerSpawn(PlayerIndex)
	currentjump[PlayerIndex] = 0
	previousjump[PlayerIndex] = 0
	timy[PlayerIndex] = 900
end

function OnTick()
	
	for i=1,16 do
		if(player_alive(i)) then
			local player = get_dynamic_player(i)

	
			vid[i] = read_bit(player + 0x11C,0)
			currentjump[i] = read_bit(player + 0x208,1)
			
			
			-- Player
			if(vid[i] == 1) then
				timy[i] = 900
			else
				ELSEACTION["else"] = 0
			end
			
			if(currentjump[i] ~= previousjump[i] and currentjump[i] == 1 and vid[i] ~= 1 and PlayerIsDriver(i)) then
				local x,y,z = 0
				local vehicle_objectid = read_dword(player + 0x11C)
				local vehicle_object = get_object_memory(vehicle_objectid)
				local x,y,z = read_vector3d(vehicle_object + 0x5C)
                wantedpos[i] = z
				timy[i] = 0
			else
				ELSEACTION["else"] = 0
			end
			
			if(currentjump[i] == 1 and vid[i] ~= 1 and PlayerIsDriver(i)) then
				
				wantedpos[i] = wantedpos[i] + movespeed
				
				local x,y,z = 0
				local vehicle_objectid = read_dword(player + 0x11C)
				local vehicle_object = get_object_memory(vehicle_objectid)
				local x,y,z = read_vector3d(vehicle_object + 0x5C)
                nowpos[i] = z
				
				tpam[i] = wantedpos[i] - nowpos[i]
				
				permi[i] = 1
			else
				ELSEACTION["else"] = 0
				permi[i] = 0
			end
			
			if(permi[i] == 1) then
				execute_command("m " .. i .. " 0 0 " .. tpam[i])
			else
				ELSEACTION["else"] = 0
			end
			
			if(currentjump[i] == previousjump[i] and currentjump[i] == 0 and vid[i] ~= 1 and timy[i] <= 20) then
				execute_command("m " .. i .. " 0 0 0.06")
				timy[i] = timy[i] + 1
			else
				ELSEACTION["else"] = 0
			end
			-- Player
			
			
			previousjump[i] = currentjump[i]

			
			
		end
	end
end

function OnDamageApplication(PlayerIndex, CauserIndex, MetaID, Damage, HitString, Backtap)
	if MetaID == distance_damage then
		if(vid[PlayerIndex] ~= 1) then
			return true, 0
		else
			return true, Damage
		end	
	end
end		

function FallID()
	distance_damage = read_dword(lookup_tag("jpt!", "globals\\distance") + 12)
end	

function PlayerIsDriver(PlayerIndex)
    local player_object = get_dynamic_player(PlayerIndex)
    local player_object_id = read_dword(get_player(PlayerIndex) + 0x34)
    local vehicleId = read_dword(player_object + 0x11C)
    if (vehicleId == 0xFFFFFFFF) then return false end
    local obj_id = get_object_memory(vehicleId)
    return read_dword(obj_id + 0x324) == player_object_id
end

function OnScriptUnload() end