-- Custom Bigass v0.1 By The -BFA- Clan

-- Inform players about Custom Bigass? Default = true

bbinform = true
bbmsg = "Bigass running with Custom Bigass"

-- Coordinates and message of hidden golden pistol.
coorx = -9.37
coory = 81.98
coorz = 13.13
discvmsg = "You have found the hidden golden pistol!"
discvallmsg = "Someone just found the hidden golden pistol!"

-- Modules
-- Enable scenery and vehicles?
enablesv = true
-- Enable hidden golden pistol?
enablep = true

-- Do not change anything below

api_version = "1.9.0.0"

spawn = {}
discovered = {}
xdis = {}
ydis = {}
zdis = {}
spawnon = {}

function OnScriptLoad()
	register_callback(cb["EVENT_GAME_START"],"Timer")
	register_callback(cb["EVENT_GAME_END"],"OffSpawn")
	register_callback(cb["EVENT_JOIN"],"bb")
	register_callback(cb["EVENT_TICK"],"Tick")
end

function Timer()
	spawnon = 0
	if(enablesv == true) then
		timer(5000, "Load")
	end
	if(enablep == true) then
		timer(50000, "PistolSpawn")
		timer(40000, "EnableSpawn")
	end
end

function EnableSpawn()
	spawnon = 1
end

function OffSpawn()
	spawnon = 0
end

function bb(PlayerIndex)
	if(bbinform == true) then
		rprint(PlayerIndex,"|c" .. bbmsg)
	end
	discovered[PlayerIndex] = 0
end

function Tick()
	for i=1,16 do
		if(player_alive(i)) then
			local player = get_dynamic_player(i)
			local x,y,z = read_vector3d(player + 0x5C)
			
			xdis[i] = x - coorx
			ydis[i] = y - coory
			zdis[i] = z - coorz
			
			if(discovered[i] == 0 and xdis[i] < 2 and ydis[i] < 2 and zdis[i] < 2 and xdis[i] > -2 and ydis[i] > -2 and zdis[i] > -2 and enablep == true) then
				say(i, discvmsg)
				say_all(discvallmsg)
				discovered[i] = 1
			end
		end
	end
end

function PistolSpawn()
	spawn_object("weap", "reach\\objects\\weapons\\pistol\\magnum\\gold magnum", coorx, coory, coorz)
	if(spawnon == 1 and enablep == true) then
		timer(30000, "PistolSpawn")
	end
end

function Load()
	execute_command("object_create turret1")
	execute_command("object_create turret2")
	execute_command("object_create kat1")
	execute_command("object_create kat2")
	execute_command("object_create mortar1")
	execute_command("object_create mortar2")
	execute_command("object_create grackle1")
	execute_command("object_create grackle2")
	spawn_object("vehi", "altis\\vehicles\\mortargoose\\mortargoose", 10.85, -39.63, 1.03, 190.14)
	spawn_object("vehi", "altis\\vehicles\\mortargoose\\mortargoose", 7.59, -40.88, 0.98, 190.14)
	spawn_object("vehi", "altis\\vehicles\\mortargoose\\mortargoose", 37.57, 3.58, 4.97, 190.14)
	spawn_object("vehi", "bourrin\\halo reach\\vehicles\\warthog\\rocket warthog", -0.59, -44.17, 1.36, -143.14)
	spawn_object("vehi", "altis\\vehicles\\mortargoose\\mortargoose", -49.92, -13.28, 1.17, -143.14)
	spawn_object("vehi", "vehicles\\newhog\\newhog mp_warthog", 128.17, 38.58, 0.50, 3.14)
	spawn_object("vehi", "vehicles\\newhog\\newhog mp_warthog", -118.85, -52.99, 0.96, 38.14)
	spawn_object("vehi", "vehicles\\falcon_destroyed\\falcon_destroyed", -80.97, -2.19, 5.97, 3.14)
	spawn_object("vehi", "vehicles\\falcon_destroyed\\falcon_destroyed", 47.52, -41.66, 4.38, 50.14)
	spawn_object("vehi", "vehicles\\falcon_destroyed\\falcon_destroyed", -43.36, 86.47, 9.30, 30.14)
	spawn_object("vehi", "vehicles\\falcon_destroyed\\falcon_destroyed", -9.54, 83.54, 13.16, 190.14)
	spawn_object("vehi", "altis\\vehicles\\scorpion_destroyed\\scorpion_destroyed", -91.94, -63.89, 1.90, 38.14)
	spawn_object("vehi", "altis\\vehicles\\scorpion_destroyed\\scorpion_destroyed", 59.59, 50.40, 5.76, 300.14)
	spawn_object("vehi", "altis\\vehicles\\scorpion_destroyed\\scorpion_destroyed", 179.79, 24.72, 1.60, 350.14)
	spawn_object("vehi", "altis\\vehicles\\scorpion_destroyed\\scorpion_destroyed", -168.56, -9.46, 9.04, 100.14)
	spawn_object("vehi", "altis\\vehicles\\truck_destroyed\\truck_destroyed", 14.38, -51.04, 3.53, 100.14)
	spawn_object("vehi", "altis\\vehicles\\truck_destroyed\\truck_destroyed", 11.82, 13.98, 5.72, 190.14)
	-- turr1
	spawn_object("vehi", "halo 4\\objects\\vehicles\\human\\turrets\\storm_unsc_artillery\\unsc_artillery_mp", 7.53, -44.74, 5.70, 3.65)
	spawn_object("vehi", "halo 4\\objects\\vehicles\\human\\turrets\\storm_unsc_artillery\\invisible\\invisible", 7.53, -44.74, 5.70, 3.65)
	-- turr2
	spawn_object("vehi", "halo 4\\objects\\vehicles\\human\\turrets\\storm_unsc_artillery\\unsc_artillery_mp", 41.89, -1.23, 5.26, 5.89)
	spawn_object("vehi", "halo 4\\objects\\vehicles\\human\\turrets\\storm_unsc_artillery\\invisible\\invisible", 41.89, -1.23, 5.26, 5.89)
	-- turr3
	spawn_object("vehi", "halo 4\\objects\\vehicles\\human\\turrets\\storm_unsc_artillery\\unsc_artillery_mp", -41.99, -18.34, 5.89, 2.95)
	spawn_object("vehi", "halo 4\\objects\\vehicles\\human\\turrets\\storm_unsc_artillery\\invisible\\invisible", -41.99, -18.34, 5.89, 2.95)
end

function OnScriptUnload() end