-- Player Double Jump v0.1 By =GG=DLBon

-- How fast should a player double jump up? Default = 0.07
jumpspeed = 0.07
-- Adjust this setting to change how much ticks can pass between pressing the space bar 2 times to double jump. Default = 10
jumpdelay = 10

-- Do you want to inform players about double jumping on join? Default = true
inform = true

-- Messages
inform_msg = "Press the spacebar twice rapidly to double jump!"

-- Do not change anything below

api_version = "1.9.0.0"

ELSEACTION = {
	["else"] = 0,
}

nowjump = {}
lastjump = {}
jumpone = {}
jumptwo = {}
permijump = {}
permijumptwo = {}
isinvehicle = {}
moretimers = {}

function OnScriptLoad()
	register_callback(cb["EVENT_TICK"],"OnTick")
	register_callback(cb['EVENT_SPAWN'],"OnPlayerSpawn")
	register_callback(cb['EVENT_JOIN'],"JoinMSGPlayer")
end

function OnPlayerSpawn(PlayerIndex)
	nowjump[PlayerIndex] = 0
	lastjump[PlayerIndex] = 0
	jumpone[PlayerIndex] = jumpdelay
	jumptwo[PlayerIndex] = 0
	permijump[PlayerIndex] = 0
	permijumptwo[PlayerIndex] = 0
	moretimers[PlayerIndex] = 0
end

function JoinMSGPlayer(PlayerIndex)
	if(inform == true) then
		rprint(PlayerIndex,"|c" .. inform_msg)
	end
end

function OnTick()
	
	for i=1,16 do
		if(player_alive(i)) then
			local player = get_dynamic_player(i)

			isinvehicle[i] = read_bit(player + 0x11C,0)
			nowjump[i] = read_bit(player + 0x208,1)
			
			if(nowjump[i] == 1 and nowjump[i] ~= lastjump[i] and jumptwo[i] == 1) then
				permijump[i] = 10
			else
				ELSEACTION["else"] = 0
			end
			
			if(nowjump[i] == 1 and nowjump[i] ~= lastjump[i] and jumpone[i] == jumpdelay and isinvehicle[i] == 1 and moretimers[i] == 0) then
				jumpone[i] = jumpone[i] - 1
				moretimers[i] = jumpdelay
				moretimers[i] = moretimers[i] * 5
			else
				ELSEACTION["else"] = 0
			end
			
			if(moretimers[i] ~= 0) then
				moretimers[i] = moretimers[i] - 1
			end
			
			if(jumpone[i] ~= jumpdelay and jumpone[i] > 0) then
				jumptwo[i] = 1
				jumpone[i] = jumpone[i] - 1
			else
				jumpone[i] = jumpdelay
				jumptwo[i] = 0
			end
			
			if(permijump[i] ~= 0) then
				permijump[i] = permijump[i] - 1
				permijumptwo[i] = 1
			else
				permijumptwo[i] = 0
			end
			
			if(permijumptwo[i] == 1) then
				execute_command("m " .. i .. " 0 0 " .. jumpspeed)
			else
				ELSEACTION["else"] = 0
			end
			-- Player
			
			lastjump[i] = nowjump[i]

			
			
		end
	end
end

function OnScriptUnload() end